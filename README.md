## Grupo 4 - EasyCook


<p align="justify">&emsp;&emsp;EasyCook é uma rede social onde os usuários colocam os ingrediente que possuem em casa como palavras chave de busca e encontram receitas com os mesmos, além de poder se cadastrar, compartilhar mais receitas e interagir com outros usuários.</p>

### Instalação


<p align="justify">&emsp;&emsp;Para instalar todas as dependências necessárias do projeto, rode o seguinte comando:</p>

```
pip install -r requirements.txt
```

<p align="justify">&emsp;&emsp;Para que seja possível a utilização da API, é preciso executar o makemigrations. Este comando é necessário para criação de novas migrações relacionadas às models:</p>

```
python3 manage.py makemigrations
```

<p align="justify">&emsp;&emsp;Para que essas migrações possam ser manipuladas, tanto na aplicação de novas quando no exclusão das existentes, é necessário executar o comando:</p>

```
python3 manage.py migrate
```

<p align="justify">&emsp;&emsp;Para executar a aplicação é fundamental o seguinte comando: </p>

```
python3 manage.py runserver
```

<p align="justify">&emsp;&emsp;Para acessar o servidor gerado no comando anterior, basta acessar [http://127.0.0.1:8000/](http://127.0.0.1:8000/)</p>
